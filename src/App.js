import './App.css';
import Header from './Components/Header';
import Homepage from './Components/Homepage';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';

function App() {
  return (
    <Router>
      <div className="App">
        <Header />
        <Switch>
          <Route exact path="/">
            <Homepage page="topstories" />
          </Route>
          <Route exact path="/news">
            <Homepage page="topstories" />
          </Route>
          <Route exact path="/new">
            <Homepage page="newstories" />
          </Route>
          <Route exact path="/best">
            <Homepage page="beststories" />
          </Route>
          <Route exact path="/show">
            <Homepage page="showstories" />
          </Route>
        </Switch>
      </div>
    </Router>
  );
}

export default App;
