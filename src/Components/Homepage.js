import React, { Component } from 'react'
import NewsListItem from './NewsListItem'


export class Homepage extends Component {
    constructor(props) {
        super(props)

        this.state = {
            isLoading: true,
            startIndex: 0,
            endIndex: 10,
        }
    }
    componentDidMount() {
        this.fetchData();
    }
    componentDidUpdate() {
        this.fetchData();
    }
    fetchData() {
        let fetchURL = "https://hacker-news.firebaseio.com/v0/" + this.props.page + ".json";
        fetch(fetchURL)
            .then(response => response.json())
            .then(result => this.setState({
                ...this.state,
                ids: result,
                isLoading: false,
            }))
            .catch(err => console.error("Error", err))
    }
    nextPage = () => {
        this.setState((previousState) => {
            previousState.startIndex += 10;
            previousState.endIndex += 10;
        })
    }
    previousPage = () => {
        this.setState((previousState) => {
            previousState.startIndex -= 10;
            previousState.endIndex -= 10;
        })
    }
    render() {
        if (!this.state.isLoading) {
            return (
                <div>
                    {this.state.ids.filter((id, index) => index >= this.state.startIndex && index < this.state.endIndex).map(id => <NewsListItem key={id} id={id} />)}
                    <div style={buttonWrapper}>
                        {this.state.startIndex > 0 && <button onClick={this.previousPage} style={buttonStyle}>Previous Page |</button>}
                        {this.state.endIndex < this.state.ids.length && <button onClick={this.nextPage} style={buttonStyle}>Next Page</button>} 
                        
                    </div>
                </div>
            )
        }
        else {
            return <p>Loading</p>
        }
    }
}
const buttonStyle = {
    border: "none",
    background: "none",
    color: "#FFFFFF",
    fontSize: "15px",
    cursor: "pointer",
}
const buttonWrapper = {
    display: "flex",
    flexDirection: "row",
    justifyContent: "center",
    background: "#FF6600",
    color: "#FFFFFF",
    padding: "10px"
}
export default Homepage
