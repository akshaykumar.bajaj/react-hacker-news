import React, { Component } from 'react'
import './newsListItem.css'

export class NewsListItem extends Component {
    constructor(props) {
        super(props)

        this.state = {
            isLoading: true,
        }
    }
    componentDidMount() {
        let fetchURL = "https://hacker-news.firebaseio.com/v0/item/" + this.props.id + ".json";
        fetch(fetchURL)
            .then(response => response.json())
            .then(result => this.setState({
                itemDetails: result,
                isLoading: false,
            }))
            .catch(err => console.error("Error", err))
    }
    convertDate = () => {
        const unixTimestamp = this.state.itemDetails.time
        const milliseconds = unixTimestamp * 1000
        const dateObject = new Date(milliseconds)
        const humanDateFormat = dateObject.toDateString()

        return humanDateFormat;
    }

    render() {
        if (this.state.isLoading) {
            return (<div>
                Loading
            </div>
            )
        } else {
            return (
                <div className="listItem">
                    <div className="firstLine">
                        <a href={this.state.itemDetails.url} className="title">{this.state.itemDetails.title}</a>
                    </div>
                    <div className="byLine">
                        <div>By {this.state.itemDetails.by}</div>
                        <div className="url">({this.state.itemDetails.url})</div>
                        <div>Published on: {this.convertDate()}</div>
                    </div>
                </div>
            )
        }
    }
}

export default NewsListItem
