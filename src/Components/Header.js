import React from 'react'
import {Link} from 'react-router-dom';

function Header() {
    const headerStyle = {
        width: "100vw",
        background: "#FF6600",
        color: "#FFFFFF",
        padding: "10px"
    
    }
    const h1Style = {
        margin : 0,
        }
    const linkStyle = {
        color: "#FFFFFF",
        textDecoration: "none"
    }
    return (
        <div style = {headerStyle}>
        <header>
           <h1 style={h1Style}> Hacker News</h1>
        </header>
        <nav>
            <Link to="/news" style={linkStyle}> Home </Link> |
            <Link to="/new" style={linkStyle}> New Stories</Link> |
            <Link to="/best" style={linkStyle}> Best Stories</Link> |
            <Link to="/show" style={linkStyle}> HN Show </Link>
        </nav>
        
    </div>
    )
}

export default Header
